extern crate config;

use std::net::{TcpStream, TcpListener};
use std::io::{Read, Write};
use std::thread;
use std::process;
use std::error::Error;
use std::collections::HashMap;

struct Config {
	port: String,
	host: String,
}

fn handle_read(mut stream: &TcpStream) {
	let mut buf = [0u8; 4096];
	match stream.read(&mut buf) {
		Ok(_) => {
			let req_str = String::from_utf8_lossy(&buf);
			println!("{}", req_str);
			}
		Err(e) => println!("Unable to read stream: {}", e),
	}	
}

fn handle_write(mut stream: TcpStream) {
	let response = b"HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF=8\r\n\r\n<html><body><h1>Hello world</body></html>\r\n";
	match stream.write(response) {
		Ok(n) => println!("Response sent: {} bytes", n),
		Err(e) => println!("Failed sending response: {}", e),
	}	
}

/**
 * fn: handle_client
 */
fn handle_client(stream: TcpStream) {
	handle_read(&stream);
	handle_write(stream);
}

fn run(listener: TcpListener) -> Result<(), Box<dyn Error>> {
	for stream in listener.incoming() {
		match stream {
			Ok(stream) => {
				thread::spawn(|| {
					handle_client(stream)
				});
			},
			// return the error here.
			Err(e) => return Result::Err(Box::new(e))
		}
	}
	Ok(())
}

fn start_listener(map: &HashMap<String, String>) -> Result<TcpListener, Box<dyn Error>> {		
	println!("{:?}", map);
	
	let port = map.get(&String::from("port")).unwrap(); 
	let host = map.get(&String::from("host")).unwrap();

	let listener = TcpListener::bind(format!("{}:{}", host, port)).unwrap_or_else(|err| {
		println!("Problem creating listener: {}", err);
		process::exit(1);
	});
	
	Ok(listener)
}

fn main() {
	
	// reference from:
	// https://github.com/mehcode/config-rs/blob/master/examples/simple/src/main.rs
	let mut settings = config::Config::default();
	settings
		// Add in `./settings.toml`
		.merge(config::File::with_name("settings")).unwrap()
		// Add in settings from the environment (with a prefix of WEBRUST)
		// Eg.. `WEBRUST_DEBUG` would set the `debug` key
		.merge(config::Environment::with_prefix("WEBRUST")).unwrap();
		
	
	let map :HashMap<String, String> = settings.try_into::<HashMap<String, String>>().unwrap();
	
	let listener = start_listener(&map).unwrap_or_else(|err| {
			println!("Unable to bind listener: {}", err);
			process::exit(1);
		});
	
	if let Err(e) = run(listener) {
		println!("Web server error: {}!", e);
		process::exit(1);
	}
}